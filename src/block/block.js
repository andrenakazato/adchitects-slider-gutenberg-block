/**
 * BLOCK: Adchitects Slider
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import "./editor.scss";
import "./style.scss";

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {
	Button,
	PanelBody,
	TextControl,
	ResponsiveWrapper,
	TextareaControl,
	HorizontalRule,
	ColorPicker
} = wp.components;
const { InspectorControls, MediaUpload, MediaUploadCheck } = wp.blockEditor;
const { Fragment } = wp.element;

/**
 * Register: Adchitects Slider Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType("grf/gutenberg-adchitects-slider", {
	title: __("Adchitects Slider"),
	icon: "smiley",
	category: "common",
	attributes: {
		sliders: {
			type: "array",
			default: []
		}
	},
	edit: props => {
		const handleSliderAdd = () => {
			const sliders = [...props.attributes.sliders];
			sliders.push({
				title: "",
				desc: "",
				color: "#FFFFFF",
				image: ""
			});
			props.setAttributes({ sliders });
		};

		const handleSliderRemove = index => {
			const sliders = [...props.attributes.sliders];
			sliders.splice(index, 1);
			props.setAttributes({ sliders });
			console.log(props.attributes.sliders);
		};

		const handleTitleChange = (title, index) => {
			const sliders = [...props.attributes.sliders];
			sliders[index].title = title;
			props.setAttributes({ sliders });
		};

		const handleDescChange = (desc, index) => {
			const sliders = [...props.attributes.sliders];
			sliders[index].desc = desc;
			props.setAttributes({ sliders });
		};

		const handleColorChange = (color, index) => {
			const sliders = [...props.attributes.sliders];
			sliders[index].color = color;
			props.setAttributes({ sliders });
		};

		const handleImgChange = (image, index) => {
			const sliders = [...props.attributes.sliders];
			sliders[index].image = image;
			props.setAttributes({ sliders });
		};

		const instructions = (
			<p>To edit the background image, you need permission to upload media.</p>
		);

		let sliderFields, sliderDisplay, sliderNav;

		if (props.attributes.sliders.length) {
			sliderFields = props.attributes.sliders.map((sliders, index) => {
				return (
					<Fragment key={index}>
						<h3>Slide {index + 1}</h3>
						<MediaUploadCheck fallback={instructions}>
							<MediaUpload
								onSelect={image => handleImgChange(image, index)}
								type="image"
								value={props.attributes.sliders[index].image}
								render={({ open }) => (
									<button
										onClick={open}
										className={
											!props.attributes.sliders[index].image
												? "editor-post-featured-image__toggle"
												: "editor-post-featured-image__preview"
										}
									>
										{!props.attributes.sliders[index].image &&
											"Set background image"}
										{props.attributes.sliders[index].image && (
											<ResponsiveWrapper
												naturalWidth={
													props.attributes.sliders[index].image.sizes.thumbnail
														.width
												}
												naturalHeight={
													props.attributes.sliders[index].image.sizes.thumbnail
														.height
												}
											>
												<img
													src={
														props.attributes.sliders[index].image.sizes
															.thumbnail.url
													}
													alt="Background image"
												/>
											</ResponsiveWrapper>
										)}
									</button>
								)}
							/>
						</MediaUploadCheck>
						<TextControl
							label="Title"
							className="ads__slider-title"
							value={props.attributes.sliders[index].title}
							onChange={title => handleTitleChange(title, index)}
						/>
						<TextareaControl
							label="Description"
							className="ads__slider-desc"
							value={props.attributes.sliders[index].desc}
							onChange={desc => handleDescChange(desc, index)}
						/>
						<p>Font Color</p>
						<ColorPicker
							color={handleColorChange}
							value={props.attributes.sliders[index].color}
							onChangeComplete={color => handleColorChange(color.hex, index)}
							disableAlpha
						/>
						<Button
							isSecondary
							className="ads__remove-slider"
							onClick={() => handleSliderRemove(index)}
						>
							Delete slider
						</Button>
						<HorizontalRule />
					</Fragment>
				);
			});

			sliderDisplay = props.attributes.sliders.map((slider, index) => {
				let styles, color;
				if (slider.image) {
					styles = {
						backgroundImage: `url(${slider.image.sizes.full.url})`,
						backgroundSize: "cover",
						backgroundPosition: "center",
						minHeight: "300px"
					};
				}

				color = {
					color: slider.color
				};

				return (
					<div className={"carousel-item active float-none"} style={styles}>
						<span class="slider-counter">Slider {index + 1}</span>
						<div class="carousel-caption d-md-block" style={color}>
							<h5>{slider.title}</h5>
							<p>{slider.desc}</p>
						</div>
					</div>
				);
			});

			sliderNav = props.attributes.sliders.map((slider, index) => {
				return (
					<li
						data-target="#ads-carousel"
						data-slide-to={index}
						className={index == 0 ? "active" : ""}
					>
						{index + 1}
					</li>
				);
			});
		}

		return [
			<InspectorControls key="1">
				<PanelBody title={__("Sliders")}>
					{sliderFields}
					<Button isDefault onClick={handleSliderAdd.bind(this)}>
						{__("Add Slider")}
					</Button>
				</PanelBody>
			</InspectorControls>,

			<div
				key="2"
				className={"carousel slide" + props.className}
				data-ride="carousel"
			>
				<div class="d-block">{sliderDisplay}</div>
			</div>
		];
	},
	save: props => {
		const sliderFields = props.attributes.sliders.map((slider, index) => {
			let styles, color;
			if (slider.image) {
				styles = {
					backgroundImage: `url(${slider.image.sizes.full.url})`,
					backgroundSize: "cover",
					backgroundPosition: "center",
					minHeight: "500px"
				};
			}

			color = {
				color: slider.color
			};

			return (
				<div
					className={"carousel-item " + (index == 0 ? "active" : "")}
					style={styles}
				>
					<div class="carousel-caption d-none d-md-block" style={color}>
						<h5>{slider.title}</h5>
						<p>{slider.desc}</p>
					</div>
				</div>
			);
		});

		const sliderNav = props.attributes.sliders.map((slider, index) => {
			return (
				<li
					data-target="#ads-carousel"
					data-slide-to={index}
					className={index == 0 ? "active" : ""}
				>
					{index + 1}
				</li>
			);
		});

		return (
			<div
				key="2"
				id="ads-carousel"
				className={"carousel slide"}
				data-ride="carousel"
			>
				<div class="carousel-inner">{sliderFields}</div>
				<div
					className={
						"carousel-controls d-flex flex-row bd-highlight m-5 position-absolute"
					}
				>
					<a
						class="carousel-control-prev position-relative"
						href="#ads-carousel"
						role="button"
						data-slide="prev"
					>
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<ol class="carousel-indicators position-relative">{sliderNav}</ol>
					<a
						class="carousel-control-next position-relative"
						href="#ads-carousel"
						role="button"
						data-slide="next"
					>
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		);
	}
});
